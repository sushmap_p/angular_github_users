## clone the code 
git clone https://github.com/bryanforbes/intern-angular
cd intern-angular

## install npm packahges 

npm install

## check npm installation 

npm --version

## install angular cli 
npm install -g @angular/cli


## run angular application using 
ng serve 

the angular application will be running in your localhost:4200

## Application explaination

1.you need to login to the application with any username and password 

2.after you will see list of github user, you can search any user in the list based on name.

3.on click of any user/record view , you will be redirected to viewrepositories page 

4.in viewrepository page you can see list of repositories of particular user 

