import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from "../shared/services/api-service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import swal from "sweetalert";
import { MatRadioChange, MatRadioButton } from "@angular/material";
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    type = "password";
  show = false;
  loginform: FormGroup;
  submitted = false;
  public checked_type;
  public stype;


    constructor(private router: Router,
        public _apiService: ApiService,
        private formBuilder: FormBuilder
        ) {}

        toggleShow() {
            this.show = !this.show;
            if (this.show) {
              this.type = "text";
            } else {
              this.type = "password";
            }
          }

    ngOnInit() {

    this.stype = "user";
    this.loginform = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", [Validators.required, Validators.minLength(6)]]
    });
    }
    get f() {
        return this.loginform.controls;
      }

      trimValue(event) {
        event.target.value = event.target.value.trim();
      }
    
      onChange(mrChange: MatRadioChange) {
        if (mrChange.value) {
          this.checked_type = mrChange.value;
        }
        let mrButton: MatRadioButton = mrChange.source;
      }
    onLogin() {
        // localStorage.setItem('isLoggedin', 'true');
        // this.router.navigate(['/dashboard']);
        this.submitted = true;
   
      
        if(this.loginform.valid){
          // localStorage.setItem("token", data.data["jwt_token"]);
          // localStorage.setItem("currentuser", JSON.stringify(data.data));
          window.location.replace("/events");
          
    }
        
       
      
   

    }
}
