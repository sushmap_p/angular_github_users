import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';
import { EventsComponent } from './events/events.component';
import { ViewreposComponent  } from './events/viewrepos/viewrepos.component'
const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'events'
            },
           
            {
                path:'events',
                component:EventsComponent
            },
            {
                path:'viewrepos',
                component:ViewreposComponent
            }

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
