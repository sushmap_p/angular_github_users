import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import { ApiService } from '../../../shared/services/api-service';
import { Router, ActivatedRoute } from '@angular/router';

import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-viewrepos',
  templateUrl: './viewrepos.component.html',
  styleUrls: ['./viewrepos.component.css']
})
export class ViewreposComponent implements OnInit {

  public list_value;
  public list_repos;
  public username;
  public userid;
  public selectedItem;
  public display_list_value;
  public edit_data;
  toggleForm: boolean = false;

  displayedColumns: string[] = ['slno', 'name','id','url','date','pushed_At'];

  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator, {}) paginator: MatPaginator;

  constructor(private _apiService: ApiService, private router: Router,private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      this.userid = params['repo_id'];
      this.username = params['repo_name']
  });
  }

  ngOnInit() {
this.listrepos()

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  listrepos(){
    this._apiService.getreposbyusername(this.userid).subscribe(data => {
      this.list_value = data["_body"];
      this.dataSource = new MatTableDataSource(this.list_value);
      console.log(JSON.parse(this.list_value));
      this.dataSource.data = JSON.parse(this.list_value);
      this.dataSource.paginator = this.paginator;
    });
  }
}
