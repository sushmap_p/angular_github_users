import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewreposComponent } from './viewrepos.component';

describe('ViewreposComponent', () => {
  let component: ViewreposComponent;
  let fixture: ComponentFixture<ViewreposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewreposComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewreposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
