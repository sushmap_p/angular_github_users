import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
// import { TranslateModule } from '@ngx-translate/core';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopnavComponent } from './components/topnav/topnav.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { PageHeaderComponent } from '../shared/modules/page-header/page-header.component';
import { MaterialModule } from '../shared/modules/material/material-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StatModule } from '../shared/modules/stat/stat.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { EventsComponent } from './events/events.component';
import { ViewreposComponent } from './events/viewrepos/viewrepos.component';


@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        MaterialModule,
        StatModule,
        FormsModule, ReactiveFormsModule,
        FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    declarations: [
        LayoutComponent, 
        TopnavComponent, 
        SidebarComponent, 
        PageHeaderComponent,
        EventsComponent,
        ViewreposComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA] 
})
export class LayoutModule {}
