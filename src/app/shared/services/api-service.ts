import { Injectable, NgModule } from '@angular/core';
import { Http, Headers, RequestOptionsArgs, RequestOptions, Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
//BackEnd access URL
// const devUrl = 'http://192.168.0.189:8003/smart_health/api/v1';
// const devUrl = 'http://192.168.0.184:8003/smart_health/api/v1';

const API_URL = 'http://localhost:9000/project_name/api/v1';
const httpOptionsImage = {
    headers: new Headers({ 'Content-Type': 'multipart/form-data', 'Authorization': localStorage.getItem('token') || '' })
};

// const httpOptionsClient = {
//     headers: new HttpHeaders({ 'Content-Type': 'application/json;charset=utf-8', 'Authorization': localStorage.getItem('token') || '' })
// };
// const httpOption = {
//     headers: new Headers({ 'Authorization': localStorage.getItem('token') || '' })
// };

// const httpOptionss = {
//     headers: new Headers({ 'Content-Type': 'application/json;charset=utf-8', 'Authorization': localStorage.getItem('token') || '' })
// };

const httpOptionsClient = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json;charset=utf-8', 'Authorization': localStorage.getItem('token') || '' })
};

const httpOptions = {
    headers: new Headers({ 'Content-Type': 'application/json;charset=utf-8' })
};

// const httpOptions = {
//     headers: new Headers({ 'Authorization': localStorage.getItem('token') || '' })
// };

const apiUrl = 'https://api.github.com';


@NgModule({ providers: [ApiService] })
@Injectable({
    providedIn: 'root'
})

export class ApiService {

    constructor(private http: Http, private httpclient: HttpClient) {

    }

    getusers(){
        return this.http.get(apiUrl + '/users');
      }
    
      getusersbyname(user){
        console.log(apiUrl + '/users:mojombo');
        
        return this.http.get(apiUrl + '/users/' + user );
      }
      getreposbyusername(user){
        return this.http.get(apiUrl + '/users/' + user +'/repos');
      }
}
